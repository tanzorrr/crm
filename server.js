/**
 * Created by alex84 on 8/1/18.
 */
var http = require('http').Server();
var io = require('socket.io')(http);
var Redis = require('ioredis');


var redis =  new Redis();
redis.subscribe('news-action');
redis.on('message',function(chanel,message){
    console.log('Message recieved: ' + message);
    console.log('Channel '+chanel)
    message =JSON.parse(message);
    io.emit(chanel + ':'+ message.event, message.data);
});

http.listen(3000,function(){
    console.log('Listing on Port: 3000');

});
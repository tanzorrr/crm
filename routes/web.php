<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth']],function(){
    Route::get('/','DashboardController@dashboard')->name('admin.index');
    Route::get('/employee/serach', 'EmployeeController@search')->name('admin.search');
    Route::get('/start/data-chart','StartController@chartData');
    Route::get('/start/socket-chart','StartController@newEvent');
    Route::get('/start/send-message','StartController@sendMessage');
    Route::resource('/employee','EmployeeController',['as'=>'admin']);

});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

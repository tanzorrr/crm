<label for="">Статус</label>
<select class="form-control" name="published">
    @if (isset($employee->id))
        <option value="0" @if ($employee->published == 0) selected="" @endif>Не опубликовано</option>
        <option value="1" @if ($employee->published == 1) selected="" @endif>Опубликовано</option>
    @else
        <option value="0">Не опубликовано</option>
        <option value="1">Опубликовано</option>
    @endif
</select>

<label for="">nmae of worker</label>
<input type="text" class="form-control" name="name" placeholder="name 0f worker" value="{{$employee->name or ""}}" required>

<label for="">photo</label>
<img src="'/photo/'.$employee->img}}" alt="">
<input type="file" class="form-control" name="img" placeholder="name 0f worker" value="{{$employee->img or ""}}" required>

<label for="">position of worker</label>
<input type="text" class="form-control" name="position" placeholder="postion 0f worker" value="{{$employee->position or ""}}" required>

<label for="">time_of_hiring</label>
<input type="text" class="form-control" name="time_of_hiring" placeholder="time_of_hiring" value="{{$employee->time_of_hiring or ""}}" required>

<label for="">Slug</label>
<input class="form-control" type="text" name="slug" placeholder="Автоматическая генерация" value="{{$employee->slug or ""}}" readonly="">

<label for="">salary</label>
<input type="text" class="form-control" name="salary" placeholder="salary" value="{{$employee->salary or ""}}" required>


<label for="">Boss</label>
<select class="form-control" name="boss_id">
    <option value="0">-- without boss --</option>
    @include('admin.employees.partials.employees', ['employees' => $employees])
</select>
<hr />

<label for="">created by</label>
<input type="text" class="form-control" name="created_by" placeholder="created_by" value="{{$employee->created_by or ""}}" required>

<label for="">modifidet by</label>
<input type="text" class="form-control" name="modifietd_by" placeholder="modifietd_by" value="{{$employee->modifietd_by or ""}}" required>




<input class="btn btn-primary " type="submit" value="Save">

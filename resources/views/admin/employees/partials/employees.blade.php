
@foreach ($employees as $employee_list)

<option value="{{$employee_list->id or ""}}"

    @isset($employee->id)

    @if($employee->boss_id == $employee_list->id)
            @selected=""

        @endif
    @if($employee->id ==$employee_list->id)
        hidden=""
    @endif
    @endisset
>

{!! $delimiter or "" !!}{{$employee_list->name or ""}}
</option>
@include('admin.employees.partials.employees',[
'employees'=>$employee_list->children,
'delimeter'=> ' - ' . $delimiter])


@endforeach
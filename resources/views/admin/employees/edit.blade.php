@extends('admin.layouts.app')

@section('content')
    <div class="container">

        <form class="form-horizontal" action="{{route('admin.employee.update',$employee)}}" method="post">
            <input type="hidden" name="_method", value="put">
            {{ csrf_field() }}

            {{-- Form include --}}
            @include('admin.employees.partials.form')
        </form>
    </div>
@endsection
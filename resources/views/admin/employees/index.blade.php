@extends('admin.layouts.app')

@section('content')
            <div class="col-lg-8">
            <a href="{{route('admin.employee.create')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o">Add worker</i></a>
            <table class="table table-striped">
                <thead>
                <th>name</th>
                <th>position</th>
                <th>time of hiring</th>
                <th>salary</th>
                <th>ection</th>
                </thead>
                <tbody>
                @forelse($employees as $employee)
                    <tr>
                        <td>{{$employee->name}}</td>
                        <td>{{$employee->position}}</td>
                        <td>{{$employee->time_of_hiring}}</td>
                        <td>{{$employee->boss_name}}</td>
                        <td>{{$employee->salary}}</td>
                        <td>
                            <form onsubmit="if(confirm('Delete?')){return true}else{return false}" action="{{route('admin.employee.destroy',$employee)}}" method="post">
                                {{--<input type="hidden" name="_method" value="DELETE">--}}


                                <a class="btn btn-info" href="{{route('admin.employee.show',$employee)}}">show</a>
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <a href="{{route('admin.employee.edit',$employee)}}"><i class="fa fa-edit"></i></a>
                                <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                            </form>

                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3" class="text-center"><h2> data is aption</h2></td>
                    </tr>
                    @endforelse
                </tbody>
                <tfoot>
                    <td colspan="3">

                    </td>
                </tfoot>
            </table>
                <chartline-component></chartline-component>
                <socket-chat-component></socket-chat-component>
                {{--<socket-component></socket-component>--}}
            </div>

            <aside-component :urldata="{{json_encode($users)}}"></aside-component>
            <ajax-component></ajax-component>
@endsection
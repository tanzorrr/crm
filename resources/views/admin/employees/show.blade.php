@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <a class="btn btn-info pull-right" href="{{route('admin.employee.index')}}">Back to list</a>
            <h1>{{$employee->name}}</h1>
                <img class="imng-fluid" src="{{asset('photo/'.$employee->img)}}" alt="">
            @if($boss)
            <h2>{{$boss->name}}</h2>
            @endif
            </div>
    </div>
@endsection
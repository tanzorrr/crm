
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue';


require('./bootstrap');

// window.Vue = require('vue');
import SearchComponent from './components/search.vue';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('search', SearchComponent);
Vue.component('aside-component', require('./components/AsideComponent.vue'));
Vue.component('ajax-component', require('./components/AjaxComponent.vue'));
Vue.component('chartline-component', require('./components/ChartlineComponent.vue'));
Vue.component('chartpie-component', require('./components/ChartpieComponet.vue'));
Vue.component('socket-chat-component', require('./components/SocetChatContainer.vue'));
//Vue.component('socket-component', require('./components/SocetComponent.vue'));

const app = new Vue({
    el: '#app'
});

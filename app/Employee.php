<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Employee extends Model
{
    protected  $fillable = ['name','img','position','time_of_hiring','slug','salary','boss_id','published','created_by','modified_by'];

    public function setSlugAttribute($value){
        $this->attributes['slug']= Str::slug(mb_substr($this->name,0, 40). "-" .\Carbon\Carbon::now()->format('dmyHi'));
    }

    public function children(){
        return $this->hasMany(self::class,'boss_id');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StartController extends Controller
{
    public  function chartData()
    {
        return[
            'labels'=>['mart','april','may','jun'],
            'datasets'=>array([
                'label'=>'Sale',
                'BackgroundColor'=>'F26202',
                'data'=>[15000,5000,10000,30000],
            ])

        ];
    }

    public  function newEvent(\illuminate\Http\Request $request){
        $request =[
            'labels'=>['mart','april','may','jun'],
            'datasets'=>array([
                'label'=>'Sale',
                'BackgroundColor'=>'F26202',
                'data'=>[15000,5000,10000,30000],
        ])
            ];
        if($request->has('label')){
            $request['label'][] =$request->input('label');
            $request['datasets'][0]['data'][]=(integer)$request->input('sale');

            if($request->has('realtime')){
                if(filter_var($request->input('realtime'),FILTER_VALIDATE_BOOLEAN)){
                    event(new\App\Events\NewEvent($result));
                }
            }
        }
    }

    public  function sendMessage(\illuminate\Http\Request $request){
        event(App\Events\NeWMessage($request->input('message')));
    }
}

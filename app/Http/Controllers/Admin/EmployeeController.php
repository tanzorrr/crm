<?php

namespace App\Http\Controllers\Admin;

use App\Employee;
use App\User;
use DeepCopy\f004\UnclonableItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use File;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        return view('admin.employees.index',
            ['employees'=> Employee::paginate(10),
                'users'=>User::all()
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        return view('admin.employees.create',[
            'employee'=>Employee::all(),
            'employees'=> Employee::with('children')->where('boss_id','0')->get(),
            'delimiter'=>'-'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $img ="";
        if($request->hasFile('img')){
         $destinationPath = 'photo';
         $file =$request->img;
          $extension=$file->getClientOriginalExtension();
          $fileName =rand(1111,9999).".".$extension;
            $file->move($destinationPath,$fileName);
            $img =$fileName;
        }


        
        Employee::create($request->all());




        return redirect()->route('admin.employee.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);
        $boss = Employee::find($employee->boss_id);


      return view('admin.employees.show',compact(['employee','boss']));
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('admin.employees.edit',[
            'employee'=>$employee,
            'employees'=> Employee::with('children')->where('boss_id','0')->get(),
            'delimiter'=>''
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $employee->update($request->except('slug'));

        return redirect()->route('admin.employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {

        $employee->delete();
        return redirect()->route('admin.employee.index');
    }

    public function search(Request $request){
$x=5;
        request()->get('search');
       $name = DB::table('employees')
            ->where(function ($query)  {

                $query->where("name", "like", "%" . request("search") . "%")
                    ->orWhere("position", "like", "%" . request("search") . "%")
                    ->orWhere("salary", "like", "%" . (int)request("search") . "%");


            })
            ->get();
dump($name);
    }
}

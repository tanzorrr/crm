<?php

use Illuminate\Database\Seeder;

class EmployTableSeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Employee::class,30)->create();
    }
}
